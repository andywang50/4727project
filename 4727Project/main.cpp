//
//  main.cpp
//  tryRCpp
//
//  Created by WangGuoan on 10/10/17.
//  Copyright © 2017 WangGuoanGuoan Wang. All rights reserved.
//

#include "elasticsearch.h"
#include "loadRData.h"
#include "TradeData.h"
#include "TickData.h"
#include "InsertMissingData.h"

#include <iostream>
//#include <RcppArmadillo.h>
#include <RInside.h>
//#include <RcppEigen.h>
#include <time.h>
#include <string>
#include <unordered_map>
#include <stdlib.h>     //for using the function sleep

void testTimeStampRounding(std::string index, std::string type){
    ElasticSearch es("localhost:9200");
    Json::Object result;
    std::string query =
    "{"
        "\"query\": {"
            "\"bool\": {"
                "\"must\":{"
                    "\"match_all\": {}"
                "},"
                "\"filter\": {"
                    "\"bool\":{"
                        "\"must\":["
                                "{\"range\": { \"transactTime\":{\"gt\": 1508964700, \"lt\":1508964800}}}"
                                "]"
                    "}"
                "}"
            "}"
        "}"
    "}\"";
    try{
        es.search(index, type, query, result);
        Json::Array temparray = result.getValue("hits").getObject().getValue("hits").getArray();
        std::cout << temparray << std::endl;
        
    }catch (Exception &msg) {
        if(strcmp(msg.what(),"Search failed.")==0 ){
            std::cout<< "Search failed. Didn't go through. " << std::endl;
        }
        else if(strcmp(msg.what(),"Search timed out.")==0){
            std::cout<< "Search timed out. " << "The timestamp is: "<< std::endl;
        }
        else{
            std::cout<< "Something weird happened. Search not successful. "<< "The timestamp is: "<<std::endl;
            
        }
    }
    
}

void testES(){
    // Instanciate elasticsearch client.
    ElasticSearch es("localhost:9200");
    
    // Index one document.
    Json::Object jData;
    jData.addMemberByKey("user", "kimchy");
    jData.addMemberByKey("post_date", "2009-11-15T14:12:12");
    jData.addMemberByKey("message", "trying out Elasticsearch");
    if(!es.index("twitter", "tweet", "1", jData))
        std::cerr << "Index failed." << std::endl;
    
    // Get document.
    Json::Object jResult;
    if(!es.getDocument("twitter", "tweet", "1", jResult))
        std::cerr << "Failed to get document." << std::endl;
    
    if(jResult["_source"].getObject() != jData)
        std::cerr << "Oups, something did not work." << std::endl;
    
    std::cout << "Great we indexed our first document: " << jResult.pretty() << std::endl;
    
    // Update document
    Json::Object jUpdateData;
    jUpdateData.addMemberByKey("user", "cpp-elasticsearch");
    if(!es.update("twitter", "tweet", "1", jUpdateData))
        std::cerr << "Failed to update document." << std::endl;
    
    // Search document.
    Json::Object jSearchResult;
    long resultSize = es.search("twitter", "tweet", "{\"query\":{\"match_all\":{}}}", jSearchResult);
    std::cout << "We found " << resultSize << " result(s):\n" << jSearchResult.pretty() << std::endl;
    
    // Delete document.
    if(!es.deleteDocument("twitter", "tweet", "1"))
        std::cerr << "Failed to delete document." << std::endl;
    
    std::cout << "First test is over. Good Bye." << std::endl;

}

void testTradeData(Rcpp::NumericMatrix& CLZ8_Data,Rcpp::DatetimeVector& CLZ8_index){
    std::unordered_map<double,TradeData> m;
    int count = 0;
    for(int i = 0; i < CLZ8_Data.nrow(); ++i){
        if(std::isnan(CLZ8_Data.row(i)[4]) == false){ //4th column is "Trade.Price"
            // Constructor of TradeData
            TradeData TD(CLZ8_index[i],CLZ8_Data.row(i),"CLZ8","CLZ8"); //CLZ8_index is a Rcpp::vector of trading times; CLZ8_Data is the Rcpp::matrix.
            auto it = m.find(CLZ8_index[i]);
            if (it == m.end()) m.emplace(CLZ8_index[i],TD);
            else it->second = TD;
            ++count;
        }

    }
    std::cout << "count:" << count << std::endl;
    std::cout << "size of hashtable:" << m.size() << std::endl;
    
}

// build a RHashTable object, print out value for a specific key
void testRHashTable(Rcpp::NumericMatrix& CLZ8_Data,Rcpp::DatetimeVector& CLZ8_index){
    RHashTable testRHT("tradedata201708","CLZ8");
    testRHT.toHashtable(CLZ8_index, CLZ8_Data,"CLZ8", "CLZ8");
    // std::cout<< testRHT.get_hashtable().at(1508964798) << std::endl;
    for (auto& x: testRHT.get_hashtable()) {
        std::cout << x.first << ": " << x.second << std::endl;
    }
}

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    
    //testES();
    
    RInside R(argc, argv);		// create an embedded R instance
    std::string path = "/Users/andy/Desktop/Fall2017/4727Programming/project/2017.10.25.CLZ8.RData";
    
    Rcpp::NumericMatrix CLZ8_Data = loadRData(path,R);
    Rcpp::DatetimeVector CLZ8_index = get_index(CLZ8_Data);
    Rcpp::CharacterVector CLZ8_colnames = get_colnames(CLZ8_Data);
    
    //simpleTest(CLZ8_Data);
    
    //testTradeData(CLZ8_Data,CLZ8_index);
    
    //testRHashTable(CLZ8_Data,CLZ8_index);
    
    // test InsertMissingData
    /*
    RHashTable rhash ("tradedata201710","CLZ8");
    rhash.toHashtable(CLZ8_index, CLZ8_Data,"CLZ8", "CLZ8");
    std::cout << "Hashtable constructed with size " << rhash.get_hashtable().size() << std::endl;
    UploadTestData(rhash);
    sleep(5);
    InsertMissingData(rhash);
    */
    
    testTimeStampRounding("tradedata201710","CLZ8");
    
    //R["txt"] = "Bye!\n";       // assign a char* (string) to 'txt'
    //R.parseEvalQ("cat(txt)");           // eval init string, ignoring returns
    
    exit(0);
    
 
    return 0;
}
