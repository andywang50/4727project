//
//  InsertMissingData.cpp
//  4727Project
//
//  Created by WangGuoan on 11/6/17.
//  Copyright © 2017 Guoan Wang. All rights reserved.
//

#include "InsertMissingData.h" // comments of these functions on in the header file.

std::set<double> CreateTestCase(ElasticSearch es, const Rcpp::NumericMatrix& RData,std::string _index, std::string _type){
    std::set<double> res;
    return res;
    
}



void UploadTestData(RHashTable rhash){
    // get index and type from RHashTable
    std::string index = rhash.get_index();
    std::string type = rhash.get_type();
    //rhash.toHashtable(CLZ8_index, CLZ8_Data,"CLZ8", "CLZ8");
    
    ElasticSearch es ("localhost:9200");
    //delete all content from ES
    es.deleteIndex(index);
    std::cout << "All deleted\n";
    
    // iterate over RHashTable, upload all except for the first three
    int i = 0;
    
    //std::ofstream out_data;
    //out_data.open("output.txt");
    for(auto& x:rhash.get_hashtable()){
        if(i%200 == 0) std::cout<< "Uploading the " << i <<"th trade data" << std::endl;
        i++;
        es.index(index, type, x.second);
        //out_data << i << ". " << x.second << "\n";
    }
    //out_data.close();
    // modify a random data field 1508964798
    Json::Object modifytest;
    std::string query = "{\"query\":{\"match\":{\"transactTime\": 1508964798}}}";
    es.search(index, type, query, modifytest);
    Json::Array temparray = modifytest.getValue("hits").getObject().getValue("hits").getArray();
    Json::Object tempobject = temparray.first().getObject();
    std::string id = tempobject.getValue("_id");
    std::string wrongprice = std::to_string(123);
    es.update(index, type, id, "price", wrongprice);
    
    std::cout <<"All uploaded"<<std::endl;
}

// std::set<long> inserted_data_timestamp
void InsertMissingData(RHashTable rhash){
    // get index and type from RHashTable
    std::string index = rhash.get_index();
    std::string type = rhash.get_type();
    // initiate ES
    ElasticSearch esinsert ("localhost:9200");
    
    // iterate over RHashTable. search - if exist, then check value matches etc. If not, insert.
    for(auto& x:rhash.get_hashtable()){
        // find the data corresponding to a timestamp, store in Json object "result"
        Json::Object result;
        std::string query = "{\"query\":{\"match\":{\"transactTime\":"+std::to_string(x.first)+"}}}";
        try{
            // get corresponding price of the timestamp in ES
            esinsert.search(index, type, query, result);
            Json::Array temparray = result.getValue("hits").getObject().getValue("hits").getArray();
            //Json::Array temparray2 = result.getValue("hits").getObject().getValue("hits").getArray();
            
            // if returns empty, the timestamp doesn't exist. Insert.
            if (temparray.empty()){
            //if (temparray.empty() && temparray2.empty()){
                //std::cout << result << std::endl;
                try{
                    esinsert.index(index, type, x.second);
                    std::cout<<"Yay. Inserted new data at timestamp："<< x.first <<std::endl;
                } catch(Exception &msg){
                    std::cout << "The index induces error." << "The timestamp is: "<<x.first<<std::endl;
                }
            }

            // if timestamp exists, compare with rdata. Update if necessary.
            else{
                Json::Object tempobject = temparray.first().getObject();
                double estradeprice = tempobject.getValue("_source").getObject().getValue("price");
                double rtradeprice = x.second.getValue("price");
                // compare es data and rdata
                if (estradeprice == rtradeprice){
                    continue;
                }
                else{
                    std::string id= tempobject.getValue("_id");
                    std::stringstream ss;
                    ss << std::setprecision(2) << std::fixed << rtradeprice;
                    std::string srtradeprice =ss.str();
                    try{
                        esinsert.update(index, type, id, "price", srtradeprice);
                        std::cout<<"Yay. Updated the trade price with timestamp " << x.first <<std::endl;
                    } catch (Exception &msg){
                        std::cout<<"The update failed. "<< "The timestamp is: "<<x.first<<std::endl;
                    }
                }
            }
            
        } catch (Exception &msg) {
            if(strcmp(msg.what(),"Search failed.")==0 ){
                std::cout<< "Search failed. Didn't go through. "<< "The timestamp is: "<<x.first<< std::endl;
            }
            else if(strcmp(msg.what(),"Search timed out.")==0){
                std::cout<< "Search timed out. " << "The timestamp is: "<<x.first<< std::endl;
            }
            else{
                std::cout<< "Something weird happened. Search not successful. "<< "The timestamp is: "<<x.first<<std::endl;
                
            }
        }
    }
}



