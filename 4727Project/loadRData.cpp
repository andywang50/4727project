//
//  loadRData.cpp
//  4727Project
//
//  Created by WangGuoan on 10/28/17.
//  Copyright © 2017 Guoan Wang. All rights reserved.
//
#include "loadRData.h"
long tz_offset_hour(time_t t) {
    struct tm local = *localtime(&t);
    struct tm utc = *gmtime(&t);
    long diff = ((local.tm_hour - utc.tm_hour) * 60 + (local.tm_min - utc.tm_min))
    * 60L + (local.tm_sec - utc.tm_sec);
    int delta_day = local.tm_mday - utc.tm_mday;
    if ((delta_day == 1) || (delta_day < -1)) {
        diff += 24L * 60 * 60;
    } else if ((delta_day == -1) || (delta_day > 1)) {
        diff -= 24L * 60 * 60;
    }
    return diff/(60*60);
}

time_t RcppTime_to_timeT(Rcpp::Datetime& t){
    long temp = static_cast<long>(t.getFractionalTimestamp());
    return (time_t) temp;
}

std::stringstream format_time_GMT(Rcpp::Datetime& t){
    const time_t timer = RcppTime_to_timeT(t);
    struct tm* gmt_time = gmtime(&timer);
    std::stringstream res;
    long relative_to_GMT =tz_offset_hour(timer);
    res << std::to_string(gmt_time->tm_year+1900) << "-" << std::to_string(gmt_time->tm_mon+1) << "-"
    << std::to_string(gmt_time->tm_mday) << "T";
    
    res << std::setw(2) << std::setfill('0') << std::to_string(gmt_time->tm_hour) << ":"
    << std::setw(2) << std::to_string(gmt_time->tm_min) << ":" << std::setw(2) << std::to_string(gmt_time->tm_sec) << "." << std::setw(3) << std::to_string(t.getMicroSeconds());
    
    res << std::string(relative_to_GMT<0?1:0,'-') << std::string(std::abs(relative_to_GMT)<10?1:0,'0') << std::to_string(std::abs(relative_to_GMT)) << ":00";
    
    return res;
}

Rcpp::NumericMatrix loadRData(std::string path, RInside R){
    R.parseEval("load(\"" + path + "\")");
    return R["CLZ8"];
}

void simpleTest(Rcpp::NumericMatrix data){
    std::cout << "the data has " << data.ncol() <<" columns, and "<< data.nrow() << " rows.\n";
    
    Rcpp::NumericVector column = data.column(1);
    std::cout << "The first element in 2nd column is:" << column[0] << std::endl;
    
    //colnames
    Rcpp::CharacterVector ch = Rcpp::colnames(data);
    std::cout << "The column names are " << ch << std::endl;
    
    //times
    Rcpp::DatetimeVector time(Rcpp::NumericVector(data.attr("index")));
    std::cout << "The first 10 indexes are:" << std::endl;
    for (int i = 0; i < 10; ++i){
        Rcpp::Datetime t = time[i];
        //std::cout << t.getFractionalTimestamp() << "\n";
        //std::cout << t.getYear() << "-" << t.getMonth() << "-" << t.getDay() << " " << t.getHours() << ":" << t.getMinutes() << ":" << t.getSeconds() << "-----" << format_time_GMT(t).str() <<"\n";
        std::cout << format_time_GMT(t).str() <<"\n";
        
    }
    
    //Rcpp::CharacterVector ch1({"Bid.Price","Bid.Size","Ask.Price","Ask.Size","Trade.Price","Volume"});
    //std::cout << "The column names1 are " << ch1 << std::endl;

    //std::cout << std::isnan(data.row(0)[4]) << std::endl;
    //std::cout << std::to_string(time[0].getFractionalTimestamp()) <<"\n";
    
}

Rcpp::DatetimeVector get_index(Rcpp::NumericMatrix data){
    Rcpp::DatetimeVector time(Rcpp::NumericVector(data.attr("index")));
    return time;
}

Rcpp::CharacterVector get_colnames(Rcpp::NumericMatrix data){
    Rcpp::CharacterVector ch = Rcpp::colnames(data);
    return ch;
}



